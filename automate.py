class State:
    def __init__(self, name, labels, is_initial=False):
        # Initialisation d'un état avec un nom, des labels et un indicateur si c'est l'état initial
        self.name = name  # Nom de l'état
        self.labels = set(labels)  # Ensemble de labels de l'état
        self.is_initial = is_initial  # Booléen indiquant si c'est l'état initial
        self.transitions = []  # Liste pour stocker les transitions à partir de cet état

    def add_transition(self, state):
        # Ajoute une transition vers un autre état
        self.transitions.append(state)

    def __repr__(self):
        # Représentation en chaîne de l'état pour l'impression, renvoie le nom de l'état
        return self.name

class Automaton:
    def __init__(self):
        # Initialisation de l'automate
        self.states = {}  # Dictionnaire pour stocker les états par leur nom
        self.initial_state = None  # Référence à l'état initial de l'automate

    def add_state(self, name, labels, is_initial=False):
        # Ajoute un nouvel état à l'automate
        state = State(name, labels, is_initial)  # Crée un nouvel état
        self.states[name] = state  # Stocke l'état dans le dictionnaire
        if is_initial:
            self.initial_state = state  # Définit l'état initial si nécessaire

    def add_transition(self, from_state, to_state):
        # Ajoute une transition de l'état from_state vers l'état to_state
        self.states[from_state].add_transition(self.states[to_state])

    def get_state(self, name):
        # Récupère un état par son nom
        return self.states.get(name)



