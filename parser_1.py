from automate import Automaton
from ctl_formula import CTLFormula

def parse_automaton(file_path):
    # Crée un nouvel automate
    automaton = Automaton()
    with open(file_path, 'r') as f:
        lines = f.readlines()

    # Premier passage : obtenir tous les états et transitions
    for line in lines:
            line = line.strip()
            if line.startswith('/*') or line == '}':
                continue
            elif line.startswith('digraph'):
                continue
            elif '[' in line and '->' not in line:
                parts = line.split('[')
                state_name = parts[0].strip()
                labels_line = parts[1].split(']')[0]
                if 'label' in labels_line:
                    labels = labels_line.split('=')[1].replace('"', '').split()
                    # Vérifier chaque label
                    filtered_labels = [label for label in labels if not label[0].isdigit() and not label.isdigit()]
                    automaton.add_state(state_name, filtered_labels)
            elif '->' in line:
                transitions = line.replace(';', '').split('->')
                for i in range(len(transitions) - 1):
                    automaton.add_transition(transitions[i].strip(), transitions[i + 1].strip())
    # Second passage : identifier l'état initial
    for line in lines:
        line = line.strip()
        if 'shape=box' in line:
            # Ligne indiquant l'état initial
            parts = line.split('[')
            state_name = parts[0].strip()
            if state_name in automaton.states:
                automaton.states[state_name].is_initial = True
                automaton.initial_state = automaton.states[state_name]
            else:
                # Si l'état n'a pas encore été créé, créez-le avec is_initial=True
                automaton.add_state(state_name, [], True)
                automaton.initial_state = automaton.states[state_name]

    if automaton.initial_state is None:
        print("Error: No initial state found.")
    else:
        print(f"Initial state: {automaton.initial_state}")
    
    # Renvoie l'automate complété    
    return automaton



def parse_ctl_formulas(file_path):
    # Ouvre le fichier contenant les formules CTL et les lit ligne par ligne
    with open(file_path, 'r') as f:
        formulas = [line.strip() for line in f.readlines() if line.strip()]
    # Parse chaque formule CTL et renvoie une liste de formules
    return [CTLFormula.parse(formula) for formula in formulas]
