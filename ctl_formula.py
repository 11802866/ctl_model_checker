class Not:
    def __init__(self, formula):
        self.formula = formula

    def evaluate(self, state, visited_states):
        # Retourner l'inverse de l'évaluation de la formule interne
        return not self.formula.evaluate(state, visited_states)

    def __repr__(self):
        return f"NOT({self.formula})"


class CTLFormula:
    def __init__(self, formula_type, operands):
        self.formula_type = formula_type
        self.operands = operands

    def __str__(self):
        if self.formula_type in ['NOT', 'EF', 'AG', 'EU']:
            return f"{self.formula_type}({', '.join(str(op) for op in self.operands)})"
        elif self.formula_type in ['AND', 'OR']:
            return f"({self.operands[0]}) {self.formula_type} ({self.operands[1]})"
        else:
            return self.formula_type

    def __repr__(self):
        return str(self)

    def evaluate(self, state, visited_states=None):
        if visited_states is None:
            visited_states = set()

        if self.formula_type == "EX":
            return any(self.operands[0].evaluate(next_state, visited_states) for next_state in state.transitions)
        elif self.formula_type == "^":
            return all(operand.evaluate(state, visited_states) for operand in self.operands)
        elif self.formula_type == "v":
            return any(operand.evaluate(state, visited_states) for operand in self.operands)
        elif self.formula_type == "NOT":
            return not self.operands[0].evaluate(state, visited_states)
        elif self.formula_type == "PROP":
            return self.operands[0] in state.labels
        elif self.formula_type == "TRUE":
            return True
        elif self.formula_type == "FALSE":
            return False
        elif self.formula_type == "EU":
            return self.exists_until(state, self.operands[0], self.operands[1], set())
        elif self.formula_type == "AU":
            return self.for_all_until(state, self.operands[0], self.operands[1], set())
        elif self.formula_type == "EG":
            return self.exists_globally(state, self.operands[0], visited_states)
        elif self.formula_type == "AG":
            return self.for_all_globally(state, self.operands[0], visited_states)
        elif self.formula_type == "EF":
            return self.exists_finally(state, self.operands[0], visited_states)

        elif self.formula_type == "AF":
            return self.for_all_finally(state, self.operands[0], visited_states)
        elif self.formula_type == "AX":
            return all(self.operands[0].evaluate(next_state, visited_states) for next_state in state.transitions)
        else:
            raise ValueError("Unsupported CTL formula type")

    def exists_until(self, state, phi, psi, visited_states):
        # Vérifie si psi est vrai dans l'état actuel, si oui, retourne vrai
        if psi.evaluate(state, visited_states):
            return True

        # Évite de revisiter les états déjà visités
        if state in visited_states:
            return False

        visited_states.add(state)
        
        # Vérifie si phi est faux dans l'état actuel, si oui, retourne faux
        if not phi.evaluate(state, visited_states):
            return False

        # Vérifie récursivement pour chaque état suivant
        for next_state in state.transitions:
            if self.exists_until(next_state, phi, psi, visited_states.copy()):
                return True

        return False

    def for_all_globally(self, state, phi, visited_states):
        # Vérifie si l'état actuel a déjà été visité, pour éviter les boucles infinies
        if state in visited_states:
            return True

        if not phi.evaluate(state, visited_states):
            return False

        visited_states.add(state)

        for next_state in state.transitions:
            if not self.for_all_globally(next_state, phi, visited_states):
                return False

        return True


    def exists_finally(self, state, phi, visited_states):
        if phi.evaluate(state, visited_states): 
            return True

        if state in visited_states:
            return False

        visited_states.add(state)

        for next_state in state.transitions:
            if self.exists_finally(next_state, phi, visited_states):
                return True

        return False


    def for_all_until(self, state, phi, psi, visited_states):
        if state in visited_states:
            return False
        
        visited_states.add(state)

        if psi.evaluate(state, visited_states):
            return True
        if not phi.evaluate(state, visited_states):
            return False

        for next_state in state.transitions:
            if not self.for_all_until(next_state, phi, psi, visited_states):
                return False
        return True

    
    def exists_globally(self, state, phi, visited_states):
        if state in visited_states:
            return False

        visited_states.add(state)

        if not phi.evaluate(state, visited_states):
            return True

        return any(self.exists_globally(next_state, phi, visited_states.copy()) for next_state in state.transitions)


    def for_all_finally(self, state, phi, visited_states):
        if state in visited_states:
            return True
        visited_states.add(state)

        if phi.evaluate(state, visited_states):
            return True

        if not state.transitions:  # Si aucun état suivant n'existe, retourner False
            return False

        for next_state in state.transitions:
            if not self.for_all_finally(next_state, phi, visited_states.copy()):
                return False
        return True

    
    @staticmethod
    def parse(formula_str):

        if formula_str[0].isdigit():
            print(f"Erreur : Les labels commençant par des chiffres ne sont pas autorisés. Formule ignorée : {formula_str}")
            return None

            
        formula_str = formula_str.strip()
       # Permet d'afficher à chaque récursion l'avancée du parsing de la formule
       # print(f"Parse formule: {formula_str}")

        if formula_str.lower() == "true":
            return CTLFormula("TRUE", [])
        elif formula_str.lower() == "false":
            return CTLFormula("FALSE", [])
        
   
         # Identifie les formules de base sans parenthèses
        if   formula_str[0].isalpha() and formula_str.replace('_', '').isalnum():
            # Vérifie si le label est un des opérateurs CTL spéciaux
            ctl_operators = ["EX", "AX", "EG", "AG", "NOT", "E", "A", "U", "AF", "EF"]
            if formula_str in ctl_operators:
                raise ValueError(f"Operateur {formula_str} est pas autorisé dans le label.")
            
            return CTLFormula("PROP", [formula_str])

        # Traite les parenthèses pour les formules complexes
        if formula_str[0] == '(':
            # Trouve la parenthèse correspondante
            count = 0
            for i, char in enumerate(formula_str):
                if char == '(':
                    count += 1
                elif char == ')':
                    count -= 1
                    if count == 0:
                        # Parenthèse correspondante trouvée
                        if i == len(formula_str) - 1:
                            # La formule entière est entre parenthèses
                            return CTLFormula.parse(formula_str[1:-1])
                        break

        # Traite les opérateurs binaires
        index = CTLFormula.find_main_operator_index(formula_str)
        if index != -1:
            operator = formula_str[index]
            if operator in ['v', '^']:
                operand1 = CTLFormula.parse(formula_str[:index])
                operand2 = CTLFormula.parse(formula_str[index+1:])
                return CTLFormula(operator, [operand1, operand2])

        # Traite les opérateurs unaires et les formules
        if formula_str.startswith('AX'):
            return CTLFormula("AX", [CTLFormula.parse(formula_str[2:])])
        if formula_str.startswith('EX'):
            return CTLFormula("EX", [CTLFormula.parse(formula_str[2:])])
        elif formula_str.startswith('NOT'):
            return CTLFormula("NOT", [CTLFormula.parse(formula_str[3:])])
        elif formula_str.startswith('EG'):
            return CTLFormula("EG", [CTLFormula.parse(formula_str[2:])])
        elif formula_str.startswith('AG'):
            return CTLFormula("AG", [CTLFormula.parse(formula_str[2:])])
        elif formula_str.startswith('EF'):
            return CTLFormula("EF", [CTLFormula.parse(formula_str[2:])])
        elif formula_str.startswith('AF'):
            return CTLFormula("AF", [CTLFormula.parse(formula_str[2:])])
        elif formula_str.startswith('E') and 'U' in formula_str:
            return CTLFormula.parse_eu_au(formula_str[1:], "EU")
        elif formula_str.startswith('A') and 'U' in formula_str:
            return CTLFormula.parse_eu_au(formula_str[1:], "AU")
        
        if '=>' in formula_str:
            parts = formula_str.split('=>')
            left = CTLFormula.parse(parts[0].strip())
            right = CTLFormula.parse(parts[1].strip())
            return CTLFormula('v', [CTLFormula('NOT', [left]), right])
        raise ValueError(f"Unsupported or incorrectly formatted CTL formula: {formula_str}")

    @staticmethod
    def find_main_operator_index(formula_str):
        # Cette fonction trouve l'index de l'opérateur principal en dehors des parenthèses
        paren_count = 0
        for i, char in enumerate(formula_str):
            if char == '(':
                paren_count += 1
            elif char == ')':
                paren_count -= 1
            elif paren_count == 0 and char in ['v', '^']:
                return i
        return -1
    @staticmethod
    def parse_eu_au(formula_str, formula_type):
        # Trouve la position du U en dehors des parenthèses
        paren_count = 0
        for i, char in enumerate(formula_str):
            if char == '(':
                paren_count += 1
            elif char == ')':
                paren_count -= 1
            elif char == 'U' and paren_count == 0:
                # La formule est divisée en deux à la position du U
                phi_str = formula_str[:i].strip()
                psi_str = formula_str[i+1:].strip()
                phi = CTLFormula.parse(phi_str)
                psi = CTLFormula.parse(psi_str)
                return CTLFormula(formula_type, [phi, psi])
        raise ValueError(f"Failed to parse {formula_type} formula: {formula_str}")
