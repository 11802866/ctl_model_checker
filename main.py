import sys
from parser_1 import parse_automaton, parse_ctl_formulas

def main(dot_file, ctl_file):
    # Analyse l'automate à partir du fichier DOT
    automaton = parse_automaton(dot_file)

    # Analyse les formules CTL à partir du fichier CTL
    formulas = parse_ctl_formulas(ctl_file)

    # Vérifie si l'automate a un état initial
    if automaton.initial_state is None:
        print("Erreur : Aucun état initial trouvé.")
        return

    # Itère sur chaque formule
    for i, formula in enumerate(formulas, start=1):
        if formula is None:
            # Si la formule est invalide (None), sautez cette itération
            continue
        
        # Évalue la formule en fonction de l'état initial de l'automate
        result = formula.evaluate(automaton.initial_state)
        
        # Affiche le résultat pour chaque formule
        print(f"Formule {i}: {'Vrai' if result else 'Faux'}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Utilisation : python main.py <automaton.dot> <formulas.ctl>")
    else:
        dot_file = sys.argv[1]
        ctl_file = sys.argv[2]
        main(dot_file, ctl_file)
